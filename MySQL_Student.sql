drop database MySQL_Student;
create database if not exists MySQL_Student;
use MySQL_Student;

CREATE TABLE Town (
	id int AUTO_INCREMENT PRIMARY KEY,
    town varchar(15) NOT NULL UNIQUE
);

CREATE TABLE Street (
	id int AUTO_INCREMENT PRIMARY KEY,
    street varchar(15) NOT NULL UNIQUE
);


CREATE TABLE Adress (
	id int AUTO_INCREMENT PRIMARY KEY,
    id_town int NULL,
    id_street int NULL,
    house varchar(15) NOT NULL,
    appartment varchar(15) NULL DEFAULT NULL,
    
    CONSTRAINT FK_Town_Adress
		FOREIGN KEY (id_town)
	REFERENCES Town (id)
	ON DELETE CASCADE ON UPDATE SET NULL,
    
    CONSTRAINT FK_Street_Adress
		FOREIGN KEY (id_street)
	REFERENCES Street (id)
	ON DELETE CASCADE ON UPDATE SET NULL
);

CREATE TABLE Specialization(
	id int AUTO_INCREMENT PRIMARY KEY,
    name varchar(15) NOT NULL UNIQUE
);

CREATE TABLE Discipline(
	id int AUTO_INCREMENT PRIMARY KEY,
	name varchar(15) NOT NULL,
    semestr ENUM('1','2','3','4','5','6','7','8') DEFAULT NULL,
    teacher varchar(45) NULL
);

CREATE TABLE Groups(
	id int AUTO_INCREMENT PRIMARY KEY,
    id_specialization int NULL,
    id_discipline int NULL,
    name varchar(15) NOT NULL,
    
    CONSTRAINT FK_Specialiazation_Groups
		FOREIGN KEY (id_specialization)
	REFERENCES Specialization (id)
	ON DELETE CASCADE ON UPDATE SET NULL,
    
    CONSTRAINT FK_Discipline_Groups
		FOREIGN KEY (id_discipline)
	REFERENCES Discipline (id)
	ON DELETE CASCADE ON UPDATE SET NULL
);

CREATE TABLE Student (
	id int AUTO_INCREMENT PRIMARY KEY,
    id_adress int NULL UNIQUE,
    id_group int NULL,
    first_name varchar(15) NOT NULL,
    last_name varchar(15) NOT NULL,
    father_name varchar(15) NOT NULL,
    birth_date datetime NOT NULL,
    photo BLOB NULL DEFAULT NULL,
    biography BLOB NULL DEFAULT NULL,
    entrance_date datetime NULL,
    current_rate DOUBLE NULL,
    
    CONSTRAINT FK_Adress_Student
		FOREIGN KEY (id_adress)
	REFERENCES Adress (id)
	ON DELETE CASCADE ON UPDATE SET NULL,
    
    CONSTRAINT FK_Groups_Student
		FOREIGN KEY (id_group)
	REFERENCES Groups (id)
	ON DELETE CASCADE ON UPDATE SET NULL
);

CREATE TABLE Scolarship(
	id int AUTO_INCREMENT PRIMARY KEY,
    id_student int,
    type ENUM('General', 'Increased') DEFAULT 'General',
    amount int NOT NULL,
    
    CONSTRAINT FK_Student_Scolarship
		FOREIGN KEY (id_student)
	REFERENCES Student (id)
	ON DELETE CASCADE ON UPDATE SET NULL
);

CREATE TABLE Student_performance (
	id int AUTO_INCREMENT PRIMARY KEY,
    id_student int NULL UNIQUE,
    id_discipline int NULL,
    passed ENUM ('Yes', 'NO') DEFAULT 'No',
    module_1_mark int NULL DEFAULT NULL,
    module_2_mark int NULL DEFAULT NULL,
    exam_mark int NULL DEFAULT NULL,
    semester_mark100 INT AS  (module_1_mark + module_2_mark + exam_mark),
    semester_mark5 INT DEFAULT NULL,
    
    CONSTRAINT FK_Performance_Student
		FOREIGN KEY (id_student)
	REFERENCES Student (id)
	ON DELETE CASCADE ON UPDATE SET NULL,
    
    CONSTRAINT FK_Performance_Discipline
		FOREIGN KEY (id_discipline)
	REFERENCES Discipline (id)
	ON DELETE CASCADE ON UPDATE SET NULL
);

insert into Town (town) values('Lviv');
insert into Town (town) values('Kyiv');
insert into Town (town) values('Dnipro');

insert into Street (street) values('Hrabianki');
insert into Street (street) values('Bandery');
insert into Street (street) values('Kalynova');

insert into Adress (id_town, id_street, house, appartment) values(1,1,34,2);
insert into Adress (id_town, id_street, house) values(2,2,43);
insert into Adress (id_town, id_street, house, appartment) values(3,3,11,32);

insert into Specialization (name) values('Kibernetika');
insert into Specialization (name) values('Ingeneria');
insert into Specialization (name) values('Kom_nauki');

insert into Discipline (name, semestr, teacher) values('Shemotehnika', '2', DEFAULT);
insert into Discipline (name, semestr, teacher) values('Basy Dannyh', '3', 'V.V.Ivanov');
insert into Discipline (name, semestr, teacher) values('Physics', '1', DEFAULT);

insert into Groups (id_specialization, id_discipline, name) values(1, 1,'EK-11');
insert into Groups (id_specialization, id_discipline, name) values(2, 1,'KI-11');
insert into Groups (id_specialization, id_discipline, name) values(2, 2,'KI-11');
insert into Groups (id_specialization, id_discipline, name) values(2, 3,'KI-11');

insert into Student (id_adress, id_group, first_name, last_name, father_name, birth_date, photo, biography, entrance_date, current_rate) values(1, 2, 'Grib', 'Tetiana', 'Petrivna', '1987-03-11', DEFAULT, DEFAULT, '2008-09-01', 4.5);
insert into Student (id_adress, id_group, first_name, last_name, father_name, birth_date, photo, biography, entrance_date, current_rate) values(2, 2, 'Vasylyv', 'Petro', 'Petrovich', '1987-11-11', DEFAULT, DEFAULT, '2008-09-01', 3.5);
insert into Student (id_adress, id_group, first_name, last_name, father_name, birth_date, photo, biography, entrance_date, current_rate) values(3, 2, 'Olkiv', 'Natalia', 'Ivanivna', '1986-10-15', DEFAULT, DEFAULT, '2008-09-01', 3.0);

insert into Scolarship (id_student, type, amount) values(1, 'Increased',2300);
insert into Scolarship (id_student, type, amount) values(2, DEFAULT,1900);
insert into Scolarship (id_student, type, amount) values(3, DEFAULT,1830);

insert into Student_performance (id_student, id_discipline, passed, module_1_mark, module_2_mark, exam_mark, semester_mark5) values(1, 1, 'Yes', 27, 28, 30, 4);
insert into Student_performance (id_student, id_discipline, passed, module_1_mark, module_2_mark, exam_mark, semester_mark5) values(2, 1, 'Yes', 20, 20, 30, 4);
insert into Student_performance (id_student, id_discipline, passed, module_1_mark, module_2_mark, exam_mark, semester_mark5) values(3, 1, 'Yes', 30, 30, 30, 5);


